<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Productos;



class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionInicio()
    {
        return $this->render('index');
    }
    
    public function actionProductos()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Productos:: find(),
        ]);
        return $this->render('productos',[
            'datos'=>$dataProvider,
        ]);
    }
    public function actionOfertas()
    {
        $dataProvider = new ActiveDataProvider([
             'query' => Productos::find()->where("oferta=1"),
             'pagination'=>[
                 'pageSize'=>2
             ]
         ]);
        
         return $this->render('productos',[
            'datos' => $dataProvider,
            ]);
        
    }
    public function actionCategorias()
    {
        return $this->render('categorias');
    }
    public function actionDondeestamos()
    {
        return $this->render('dondeestamos');
    }
    
}
