<?php
use yii\helpers\Html;
?>
<div class="col-lg-6">
    <div class="thumbnail">
      <?= Html::img("@web/imgs/" . $model->foto); ?>
      <div class="caption">
        <h3><?= $model->nombre ?></h3>
        <p><?= $model->descripcion ?></p>
      </div>
    </div>
  </div>
