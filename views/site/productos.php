<?php
    use yii\widgets\ListView;
?>

<div class="row">
    <?= ListView::widget([
        'dataProvider' => $datos,
        'itemView' => '_producto',
    ]); ?>
</div>
