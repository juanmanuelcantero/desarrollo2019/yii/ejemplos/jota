<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron jumbotron-fluid">
        <h1 class="display-4">Ejemplo 1 de aplicación</h1>

        <p class="lead">Podemos ver un ejemplo del funcionamiento de este framework</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-sm-6">
                
                <div class="card">
                    <img src="http://placehold.it/550x250" class="card-img-top" alt="imagen">
                    <h2 class="card-title">Titulo</h2>

                    <p class="card-text">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur.</p>
                </div>

            </div>
            
            <div class="col-sm-6">
                
                <div class="card">
                    <img src="http://placehold.it/550x250" class="card-img-top" alt="imagen">
                    <h2 class="card-title">Titulo</h2>

                    <p class="card-text body-content">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur.</p>
                </div>
            </div>
        </div>

    </div>
</div>