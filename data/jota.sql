﻿--
-- Definition for database ejemplo1yii2
--
DROP DATABASE IF EXISTS jota;
CREATE DATABASE IF NOT EXISTS jota
	CHARACTER SET utf8
	COLLATE utf8_spanish_ci;

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

-- 
-- Set default database
--

﻿USE jota;

--
-- Definition for table noticias
--
CREATE TABLE IF NOT EXISTS productos (
  id INT(11) NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(255),
  foto VARCHAR(255),
  descripcion VARCHAR(255),
  oferta BOOLEAN,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 5
AVG_ROW_LENGTH = 4096
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

-- 
-- Dumping data for table noticias
--

INSERT INTO productos VALUES
  ('1','camisa','camisa.jpg','Camisa con tecnología DriLook® con efecto antisudor. Slim-fit, estilo formal.','false'),
  ('2','camiseta','camiseta.jpg','Camiseta de manga corta con logo para hombre. Parche con bandera de Tommy Hilfiger bordado en el pecho. Fit regular.','true'),
  ('3','pantalon','pantalon.jpg','Pantalón largo hombre ideal para la practica deportiva y uso urbano.','true'),
  ('4','sudadera','sudadera.jpg','Sudadera sin capucha de cuello redondo diseñada en bloques de colores.Logotipo de la marca en la parte frontal.','false'),
  ('5','gorra','gorra.jpg','Gorra 6 paneles color en microfibra/poliéster Blazok. Gorra de color con 6 paneles en microfibra/poliéster. Cierre ajustable con velcro.','true');

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;